package self.azterix87.wizardgame;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.math.Vector2;
import self.azterix87.seraphim.GameContext;
import self.azterix87.seraphim.Timer;

import java.util.Random;

/**
 * Created by azterix87 on 1/25/17.
 * Project WizardGame
 */
public class Weapon {
    protected String name;
    protected Timer coolDown;
    protected Random random;
    protected int minDamage;
    protected int maxDamage;

    public Weapon(String name, float coolDown, int minDamage, int maxDamage) {
        this(name, coolDown);
        this.minDamage = minDamage;
        this.maxDamage = maxDamage;
        random = new Random();
    }

    public Weapon(String name, float coolDown) {
        this.name = name;
        this.coolDown = new Timer(coolDown);
    }

    public void fire(GameContext gameContext, Entity owner, Vector2 position, Vector2 attackDirection) {
        coolDown.reset();
    }

    public boolean isReady() {
        return coolDown.isFinished();
    }

    protected int getRandomDamage() {
        return minDamage + random.nextInt(maxDamage - minDamage);
    }

    public Timer getCoolDown() {
        return coolDown;
    }

    public void setCoolDown(Timer coolDown) {
        this.coolDown = coolDown;
    }

    public void setCoolDown(float coolDown) {
        this.coolDown = new Timer(coolDown);
    }

    public int getMinDamage() {
        return minDamage;
    }

    public void setMinDamage(int minDamage) {
        this.minDamage = minDamage;
    }

    public int getMaxDamage() {
        return maxDamage;
    }

    public void setMaxDamage(int maxDamage) {
        this.maxDamage = maxDamage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
