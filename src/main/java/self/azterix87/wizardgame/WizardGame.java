package self.azterix87.wizardgame;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.SkinLoader;
import com.badlogic.gdx.assets.loaders.resolvers.LocalFileHandleResolver;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGeneratorLoader;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import self.azterix87.seraphim.ScreenManager;
import self.azterix87.seraphim.assets.AssetType;
import self.azterix87.seraphim.assets.Assets;
import self.azterix87.seraphim.assets.AssetsLoader;
import self.azterix87.seraphim.systems.motionanimation.MotionAnimation;
import self.azterix87.seraphim.systems.motionanimation.MotionAnimationLoader;
import self.azterix87.seraphim.systems.spriterendering.AnimationLoader;

/**
 * Created by azterix87 on 6/1/2017.
 * Project WizardGame
 */

public class WizardGame extends Game {
    public static final String ASSETS_FOLDER = "assets";
    public static boolean debug = true;
    private ScreenManager screenManager;
    private Assets assets;


    @Override
    public void create() {
        Gdx.app.setLogLevel(Application.LOG_DEBUG);
        Gdx.app.log("@WizardGame", "WizardGame starting...");
        Gdx.app.log("@WizardGame", "Loading assets");
        loadAssets();
        initAssets();
        Gdx.app.log("@WizardGame", "Setting up screens");
        setUpScreens();
        Gdx.app.log("@WizardGame", "Entering main menu");
        screenManager.setCurrentScreen("mainMenu");
    }

    private void loadAssets() {
        AssetManager assetManager = new AssetManager(new LocalFileHandleResolver());
        AssetsLoader assetsLoader = new AssetsLoader();

        assetsLoader.registerAssetType(new AssetType("Texture", Texture.class, ".jpg", ".png"));
        assetsLoader.registerAssetType(new AssetType("Texture Atlas", TextureAtlas.class, ".atlas"));
        assetsLoader.registerAssetType(new AssetType("Animation", Animation.class,
                new AnimationLoader(new LocalFileHandleResolver()), ".animation"));
        assetsLoader.registerAssetType(new AssetType("Motion animation", MotionAnimation.class,
                new MotionAnimationLoader(new LocalFileHandleResolver()), ".manim"));
        assetsLoader.registerAssetType(new AssetType("Font generator", FreeTypeFontGenerator.class,
                new FreeTypeFontGeneratorLoader(new LocalFileHandleResolver()), ".ttf"));
        assetsLoader.registerAssetType(new AssetType("UI Skin", Skin.class,
                new SkinLoader(new LocalFileHandleResolver()), ".json"));
        assetsLoader.loadAssets(new FileHandle(ASSETS_FOLDER), assetManager);
        assets = new Assets(assetManager);
    }

    private void initAssets() {
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 12;
        parameter.borderWidth = 1;
        parameter.borderColor = Color.BLACK;
        assets.fontManager.generateAndRegisterFont("regular", "assets/fonts/a_AvanteBs.ttf", parameter);
    }

    private void setUpScreens() {
        screenManager = new ScreenManager(this);
        screenManager.addScreen("mainMenu", new ScreenMainMenu(assets, screenManager));
        screenManager.addScreen("game", new ScreenGame(this, screenManager, assets));
    }

    @Override
    public void dispose() {
        super.dispose();
        assets.dispose();
        screenManager.dispose();
    }


}
