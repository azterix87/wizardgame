package self.azterix87.wizardgame;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import self.azterix87.seraphim.Controller;
import self.azterix87.seraphim.GameData;
import self.azterix87.seraphim.ScreenManager;
import self.azterix87.seraphim.assets.Assets;
import self.azterix87.seraphim.systems.RenderingSystem;
import self.azterix87.seraphim.systems.collision.CollisionSystem;
import self.azterix87.seraphim.systems.dynamics.DynamicsSystem;
import self.azterix87.seraphim.systems.fonts.FontRenderingModule;
import self.azterix87.seraphim.systems.lighting.LightingModule;
import self.azterix87.seraphim.systems.motionanimation.MotionAnimationSystem;
import self.azterix87.seraphim.systems.physics.PhysicsSystem;
import self.azterix87.seraphim.systems.spriterendering.SpriteRenderingModule;
import self.azterix87.seraphim.tiledmap.Level;
import self.azterix87.seraphim.tiledmap.LevelRenderingModule;
import self.azterix87.wizardgame.levelgen.Artifact;
import self.azterix87.wizardgame.levelgen.LevelGenerator;

/**
 * Created by azterix87 on 6/1/2017.
 * Project WizardGame
 */
public class Game {
    private GameData gameData;
    private WizardGame wizardGame;
    private ScreenManager screenManager;
    private OrthographicCamera playerCam;
    private FollowingCameraController playerCamController;
    private Level level;
    private LevelController levelController;
    private Entity player;
    private PlayerController playerControler;
    private boolean paused;

    public Game(WizardGame wizardGame, ScreenManager screenManager, Assets assets) {
        this.wizardGame = wizardGame;
        this.screenManager = screenManager;
        this.gameData = new GameData(wizardGame, assets, new SpriteBatch());

        this.playerCam = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        this.playerCamController = new FollowingCameraController(playerCam, 0.3F);
        this.gameData.setMainCamera(playerCam);
        this.gameData.addGlobalController(playerCamController);
    }

    public void init() {
        Gdx.app.log("@Game", "Initialising collision and physics systems");
        gameData.entityEngine.addSystem(new PhysicsSystem(0));
        gameData.entityEngine.addSystem(new CollisionSystem(1));
        gameData.entityEngine.addSystem(new DynamicsSystem(2));

        Gdx.app.log("@Game", "Initialising rendering and animation systems");
        gameData.entityEngine.addSystem(new MotionAnimationSystem(3, gameData));

        RenderingSystem renderingSystem = new RenderingSystem(5, gameData);

        renderingSystem.addModule(new SpriteRenderingModule());
        renderingSystem.addModule(new LightingModule(new Vector3(0.07F, 0.07F, 0.07F)));
        renderingSystem.addModule(new FontRenderingModule());
        LevelRenderingModule levelRenderingModule = new LevelRenderingModule();
        //levelRenderingModule.setLayerOffsetYMul(24);
        renderingSystem.addModule(levelRenderingModule);


        gameData.entityEngine.addSystem(renderingSystem);

        if (WizardGame.debug) {
            gameData.entityEngine.addSystem(new DebugRenderingSystem(gameData));
        }

        Gdx.app.log("@Game", "Initialising entity factories");
        gameData.factoryManager.registerFactory(new PlayerFactory());
        gameData.factoryManager.registerFactory(new DecorationFactory());


        //Init debug controller
        gameData.addGlobalController(new DebugController(0, gameData));
    }

    public void startGame() {
        initLevel();
        initPlayer();
    }

    private void initLevel() {
        level = new Level(Tiles.getTileRegistry(), gameData);

        Entity levelEntity = new Entity();
        levelEntity.add(level);
        gameData.addEntity(levelEntity);

        LevelGenerator generator = new LevelGenerator(System.nanoTime());
        levelController = new LevelController(level, generator, gameData);
        levelController.generate();
        gameData.addGlobalController(levelController);
    }


    private void initPlayer() {
        Artifact spawnpoint = levelController.getLevelData().findArtifactByType("player_spawn");
        player = gameData.spawnEntity("player", spawnpoint.position.x, spawnpoint.position.y, null);
        playerCamController.setTarget(player);
        gameData.addGlobalController(new PlayerController(gameData, player));
    }

    public void updateAndRender(float delta) {
        for (Controller controller : gameData.getControllers()) {
            controller.update(delta);
        }
        gameData.entityEngine.update(delta);
    }


    public void pause() {
        paused = true;
    }

    public void resume() {
        paused = false;
    }

    public boolean isPaused() {
        return paused;
    }

    public void resize(int width, int height) {
        OrthographicCamera cam = (OrthographicCamera) gameData.getMainCamera();
        float coef = ((float) width) / height;
        cam.viewportWidth = cam.viewportHeight * coef;
        cam.update();
    }

    public InputProcessor getInputProcessor() {
        return gameData.getInputProcessor();
    }
}
