package self.azterix87.wizardgame;

import self.azterix87.seraphim.tiledmap.Tile;
import self.azterix87.seraphim.tiledmap.TileRegistry;

/**
 * Created by azterix87 on 6/26/2017.
 * Project WizardGame
 */

/**
 * Utility class storing all tiles data(Has to be raplaced with xml file)
 */
public class Tiles {
    public static Tile TILE_FLOOR = new Tile("tile.floor", "assets/textures/level.atlas", "floor");
    public static Tile TILE_FLOOR_TOP = new Tile("tile.floor_top", "assets/textures/level.atlas", "floor_top");
    public static Tile TILE_WALL_BOTTOM_LEFT = new Tile("tile.wall_bottom_left", "assets/textures/level.atlas", "wall_bottom_left");
    public static Tile TILE_WALL_BOTTOM_MIDDLE = new Tile("tile.wall_bottom_middle", "assets/textures/level.atlas", "wall_bottom_middle");
    public static Tile TILE_WALL_BOTTOM_RIGHT = new Tile("tile.wall_bottom_right", "assets/textures/level.atlas", "wall_bottom_right");
    public static Tile TILE_WALL_LEFT = new Tile("tile.wall_left", "assets/textures/level.atlas", "wall_left");
    public static Tile TILE_WALL_MIDDLE = new Tile("tile.wall_middle", "assets/textures/level.atlas", "wall_middle");
    public static Tile TILE_WALL_RIGHT = new Tile("tile.wall_right", "assets/textures/level.atlas", "wall_right");
    public static Tile TILE_WALL_BACK = new Tile("tile.wall_back", "assets/textures/level.atlas", "void");
    public static Tile TILE_ROOF_BOTTOM = new Tile("tile.roof_bottom", "assets/textures/level.atlas", "roof_bottom");
    public static Tile TILE_ROOF_BOTTOM_LEFT = new Tile("tile.roof_bottom_left", "assets/textures/level.atlas", "roof_bottom_left");
    public static Tile TILE_ROOF_BOTTOM_RIGHT = new Tile("tile.roof_bottom_right", "assets/textures/level.atlas", "roof_bottom_right");
    public static Tile TILE_ROOF_TOP = new Tile("tile.roof_top", "assets/textures/level.atlas", "roof_top");
    public static Tile TILE_ROOF_TOP_LEFT = new Tile("tile.roof_top_left", "assets/textures/level.atlas", "roof_top_left");
    public static Tile TILE_ROOF_TOP_RIGHT = new Tile("tile.roof_top_right", "assets/textures/level.atlas", "roof_top_right");
    public static Tile TILE_ROOF_RIGHT = new Tile("tile.roof_right", "assets/textures/level.atlas", "roof_middle_right");
    public static Tile TILE_ROOF_LEFT = new Tile("tile.roof_left", "assets/textures/level.atlas", "roof_middle_left");
    public static Tile TILE_ROOF_INNER_BOTTOM_LEFT = new Tile("tile.roof_inner_bottom_left", "assets/textures/level.atlas", "roof_inner_bottom_left");
    public static Tile TILE_ROOF_INNER_BOTTOM_RIGHT = new Tile("tile.roof_inner_bottom_right", "assets/textures/level.atlas", "roof_inner_bottom_right");
    public static Tile TILE_ROOF_INNER_TOP_LEFT = new Tile("tile.roof_inner_top_left", "assets/textures/level.atlas", "roof_inner_top_left");
    public static Tile TILE_ROOF_INNER_TOP_RIGHT = new Tile("tile.roof_inner_top_right", "assets/textures/level.atlas", "roof_inner_top_right");
    public static Tile TILE_ROOF_EMPTY = new Tile("tile.roof_empty", "assets/textures/level.atlas", "void");

    public static TileRegistry getTileRegistry() {
        TileRegistry tileRegistry = new TileRegistry(128);
        tileRegistry.addTile(1, TILE_FLOOR);
        tileRegistry.addTile(2, TILE_FLOOR_TOP);
        tileRegistry.addTile(3, TILE_WALL_BOTTOM_LEFT);
        tileRegistry.addTile(4, TILE_WALL_BOTTOM_MIDDLE);
        tileRegistry.addTile(5, TILE_WALL_BOTTOM_RIGHT);
        tileRegistry.addTile(6, TILE_WALL_LEFT);
        tileRegistry.addTile(7, TILE_WALL_MIDDLE);
        tileRegistry.addTile(8, TILE_WALL_RIGHT);
        tileRegistry.addTile(9, TILE_WALL_BACK);
        tileRegistry.addTile(10, TILE_ROOF_BOTTOM);
        tileRegistry.addTile(11, TILE_ROOF_BOTTOM_LEFT);
        tileRegistry.addTile(12, TILE_ROOF_BOTTOM_RIGHT);
        tileRegistry.addTile(13, TILE_ROOF_TOP);
        tileRegistry.addTile(14, TILE_ROOF_TOP_LEFT);
        tileRegistry.addTile(15, TILE_ROOF_TOP_RIGHT);
        tileRegistry.addTile(16, TILE_ROOF_RIGHT);
        tileRegistry.addTile(17, TILE_ROOF_LEFT);
        tileRegistry.addTile(18, TILE_ROOF_INNER_BOTTOM_LEFT);
        tileRegistry.addTile(19, TILE_ROOF_INNER_BOTTOM_RIGHT);
        tileRegistry.addTile(20, TILE_ROOF_INNER_TOP_LEFT);
        tileRegistry.addTile(21, TILE_ROOF_INNER_TOP_RIGHT);
        tileRegistry.addTile(22, TILE_ROOF_EMPTY);
        return tileRegistry;
    }
}

