package self.azterix87.wizardgame;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.math.Vector2;
import self.azterix87.seraphim.GameContext;
import self.azterix87.seraphim.components.LivingStats;
import self.azterix87.seraphim.systems.physics.PhysicalBody;

/**
 * Created by azterix87 on 1/26/17.
 * Project WizardGame
 */
public class WeaponMelee extends Weapon {
    private static final float knockBackMultiplier = 100;
    private float attackDistance;
    private float knockBackImp;
    private String targetTag;

    public WeaponMelee(float coolDown, int minDamage, int maxDamage, float attackDistance, float knockBack) {
        this(coolDown, minDamage, maxDamage, attackDistance, knockBack, "mob");
    }

    public WeaponMelee(float coolDown, int minDamage, int maxDamage, float attackDistance, float knockBack, String targetTag) {
        super("Melee", coolDown, minDamage, maxDamage);
        this.attackDistance = attackDistance;
        this.knockBackImp = knockBack * knockBackMultiplier;
        this.targetTag = targetTag;
    }

    @Override
    public void fire(GameContext gameContext, Entity owner, Vector2 position, Vector2 attackDirection) {
        super.fire(gameContext, owner, position, attackDirection);
        Entity target = gameContext.raytrace(position, attackDirection, attackDistance, targetTag);
        if (target != null) {
            LivingStats.mapper.get(target).damage(getRandomDamage());
            PhysicalBody.mapper.get(target).applyImpulse(attackDirection.cpy().scl(knockBackImp));
        }
    }
}
