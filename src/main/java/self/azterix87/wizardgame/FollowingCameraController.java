package self.azterix87.wizardgame;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import self.azterix87.seraphim.Controller;
import self.azterix87.seraphim.GameContext;
import self.azterix87.seraphim.components.Transform;

/**
 * Created by azterix87 on 1/7/2017.
 * Project WizardGame
 */
//TODO REWRITE THIS CLASS
public class FollowingCameraController implements Controller {
    private Camera camera;
    private Entity target;
    private float smoothness;
    private Vector2 currentPos = new Vector2();

    public FollowingCameraController(Camera camera, float smoothness) {
        this.camera = camera;
        this.smoothness = smoothness;
    }

    @Override
    public void update(float delta) {
        if (target == null) {
            return;
        }

        Vector3 targetPos = Transform.mapper.get(target).localPosition;
        Vector2 displace = new Vector2(targetPos.x, targetPos.y).sub(currentPos);

        if (displace.len() < 1) {
            return;
        }

        displace.scl(delta / smoothness);
        if (displace.len() < 1) {
            displace.nor();
        }

        currentPos.add(displace);

        camera.position.set(currentPos.x, currentPos.y, camera.position.z);
        //camera.localPosition.set((int)camera.localPosition.x, (int)camera.localPosition.y, camera.localPosition.z);
        camera.update();
    }

    @Override
    public void onControllerAdded(GameContext gameContext) {

    }

    @Override
    public void onControllerRemoved(GameContext gameContext) {

    }

    public Entity getTarget() {
        return target;
    }

    public void setTarget(Entity target) {
        this.target = target;
    }
}
