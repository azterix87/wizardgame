package self.azterix87.wizardgame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import self.azterix87.seraphim.Controller;
import self.azterix87.seraphim.GameContext;
import self.azterix87.seraphim.tiledmap.Chunk;
import self.azterix87.seraphim.tiledmap.Level;
import self.azterix87.seraphim.tiledmap.Tile;
import self.azterix87.wizardgame.levelgen.*;

/**
 * Created by azterix87 on 6/26/2017.
 * Project WizardGame
 */
public class LevelController implements Controller {
    private static final int CHUNK_WIDTH = 24;
    private static final int CHUNK_HEIGHT = 24;
    private static final int fieldOfVision = 3;
    private Level level;
    private LevelGenerator levelGenerator;
    private LevelData levelData;
    private int currentChunkX, currentChunkY;
    private GameContext gameContext;


    public LevelController(Level level, LevelGenerator levelGenerator, GameContext gameContext) {
        this.level = level;
        this.levelGenerator = levelGenerator;
        this.gameContext = gameContext;
    }

    public void generate() {
        Gdx.app.log("@LevelController", "Generating level...");
        levelGenerator.generate(30);
        levelData = levelGenerator.getLevelData();
        levelData.scale(2);
        LevelProcessor processor = new LevelProcessor(levelData);
        processor.removeDeadEnds();
        processor.setSpawnPoint();

        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                loadChunk(i, j);
            }
        }
    }


    private void loadChunk(int chunkX, int chunkY) {
        BaseMap baseMap = levelData.getBasemap(chunkX * CHUNK_WIDTH - 1, chunkY * CHUNK_HEIGHT - 1, CHUNK_WIDTH + 2, CHUNK_HEIGHT + 2);
        baseMap.setOffsetX(-1);
        baseMap.setOffsetY(-1);
        Chunk chunk = new ChunkGenerator(chunkX * CHUNK_WIDTH, chunkY * CHUNK_HEIGHT, CHUNK_WIDTH, CHUNK_HEIGHT, baseMap, level.getTileRegistry()).generate();
        level.addChunk(chunk);
    }

    private void unloadChunk(int chunkX, int chunkY) {
        Chunk chunk = level.getChunkAt(chunkX * CHUNK_WIDTH, chunkY * CHUNK_HEIGHT);
        if (chunk != null) {
            level.removeChunk(chunk);
        }
    }

    private void moveNorth() {
        for (int i = 0; i < fieldOfVision; i++) {
            unloadChunk(currentChunkX - fieldOfVision / 2 + i, currentChunkY - fieldOfVision / 2);
            loadChunk(currentChunkX - fieldOfVision / 2 + i, currentChunkY + fieldOfVision / 2 + 1);
        }
        currentChunkY++;
    }

    private void moveSouth() {
        for (int i = 0; i < fieldOfVision; i++) {
            unloadChunk(currentChunkX - fieldOfVision / 2 + i, currentChunkY + fieldOfVision / 2);
            loadChunk(currentChunkX - fieldOfVision / 2 + i, currentChunkY - fieldOfVision / 2 - 1);
        }
        currentChunkY--;
    }

    private void moveEast() {
        for (int i = 0; i < fieldOfVision; i++) {
            unloadChunk(currentChunkX - fieldOfVision / 2, currentChunkY - fieldOfVision / 2 + i);
            loadChunk(currentChunkX + fieldOfVision / 2 + 1, currentChunkY - fieldOfVision / 2 + i);
        }
        currentChunkX++;
    }

    private void moveWest() {
        for (int i = 0; i < fieldOfVision; i++) {
            unloadChunk(currentChunkX + fieldOfVision / 2, currentChunkY - fieldOfVision / 2 + i);
            loadChunk(currentChunkX - fieldOfVision / 2 - 1, currentChunkY - fieldOfVision / 2 + i);
        }
        currentChunkX--;
    }


    @Override
    public void update(float delta) {
        Camera camera = gameContext.getMainCamera();
        int camX = Math.floorDiv((int) camera.position.x, Tile.SIZE * CHUNK_WIDTH);
        int camY = Math.floorDiv((int) camera.position.y, Tile.SIZE * CHUNK_WIDTH);
        if (camX > currentChunkX) {
            moveEast();
        }
        if (camX < currentChunkX) {
            moveWest();
        }
        if (camY > currentChunkY) {
            moveNorth();
        }
        if (camY < currentChunkY) {
            moveSouth();
        }
    }

    public LevelData getLevelData() {
        return levelData;
    }

    @Override
    public void onControllerAdded(GameContext gameContext) {

    }

    @Override
    public void onControllerRemoved(GameContext gameContext) {

    }
}
