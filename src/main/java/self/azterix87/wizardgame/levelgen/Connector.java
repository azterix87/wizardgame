package self.azterix87.wizardgame.levelgen;

/**
 * Created by azterix87 on 6/5/2017.
 * Project WizardGame
 */
public class Connector {
    public final int x, y;
    public final Direction direction;
    public final boolean fixedDirection;
    public final LevelNode owner;

    public Connector(int x, int y, LevelNode owner, boolean fixedDirection, Direction direction) {
        this.x = x;
        this.y = y;
        this.owner = owner;
        this.direction = direction;
        this.fixedDirection = fixedDirection;
    }
}
