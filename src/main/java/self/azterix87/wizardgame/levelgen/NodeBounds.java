package self.azterix87.wizardgame.levelgen;

/**
 * Created by azterix87 on 6/7/2017.
 * Project WizardGame
 */
public class NodeBounds {
    private int x, y;
    private int width, height;
    private int boarder;

    public NodeBounds(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public NodeBounds(NodeBounds src) {
        this.x = src.x;
        this.y = src.y;
        this.width = src.width;
        this.height = src.height;
    }

    public boolean intersects(NodeBounds another) {
        int maxBoarder = Math.max(boarder, another.getBoarder());
        return another.x <= getEndX() + maxBoarder && another.y <= getEndY() + maxBoarder && another.getEndX() >= x - maxBoarder && another.getEndY() >= y - maxBoarder;
    }

    public boolean intersectsIgnoreBoarder(NodeBounds another) {
        return another.x <= getEndX() && another.y <= getEndY() && another.getEndX() >= x && another.getEndY() >= y;
    }

    public boolean contains(int x, int y) {
        return x >= this.x && y >= this.y && x <= getEndX() && y <= getEndY();
    }

    public void scaleAndMove(int scale) {
        x = x * scale;
        y = y * scale;
        width = width * scale;
        height = height * scale;
    }

    /**
     * grow in all directions
     */
    public void grow(int amount) {
        growNorth(amount);
        growEast(amount);
        growWest(amount);
        growSouth(amount);
    }

    public void grow(Direction direction, int amount) {
        switch (direction) {
            case NORTH:
                growNorth(amount);
                break;
            case EAST:
                growEast(amount);
                break;
            case SOUTH:
                growSouth(amount);
                break;
            case WEST:
                growWest(amount);
                break;
        }
    }

    public void growNorth(int amount) {
        height += amount;
    }

    public void growEast(int amount) {
        width += amount;
    }

    public void growSouth(int amount) {
        y -= amount;
        height += amount;
    }

    public void growWest(int amount) {
        x -= amount;
        width += amount;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getEndX() {
        return x + width - 1;
    }

    public int getEndY() {
        return y + height - 1;
    }

    public int getBoarder() {
        return boarder;
    }

    public void setBoarder(int boarder) {
        this.boarder = boarder;
    }

    @Override
    public String toString() {
        return "NodeBounds{" +
                "x=" + x +
                ", y=" + y +
                ", width=" + width +
                ", height=" + height +
                ", endX=" + getEndX() +
                ", endY=" + getEndY() +
                '}';
    }
}
