package self.azterix87.wizardgame.levelgen;

import java.util.Random;

/**
 * Created by azterix87 on 6/3/2017.
 * Project WizardGame
 */
public class Dice {
    private int min, max;
    private Random random;

    public Dice(int min, int max, Random random) {
        this.min = min;
        this.max = max;
        this.random = random;
    }

    public int next() {
        return min + random.nextInt(max - min + 1);
    }
}
