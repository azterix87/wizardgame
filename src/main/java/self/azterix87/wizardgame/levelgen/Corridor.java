package self.azterix87.wizardgame.levelgen;

/**
 * Created by azterix87 on 6/3/2017.
 * Project WizardGame
 */
public class Corridor extends LevelNode {
    private int startX, startY;
    private Direction direction;
    private int length;

    public Corridor(LevelGenerator levelGenerator, Connector connector) {
        super(levelGenerator, connector);
        this.startX = connector.x;
        this.startY = connector.y;
        this.length = 1;
        determineDirection(connector);
    }

    @Override
    public void generate() {
        int desiredLength = levelGenerator.corridorLengthDice.next();

        //Determine if we have any nodes on our way
        int curX = startX, curY = startY;
        LevelNode targetNode = null;

        for (int i = 1; i <= desiredLength; i++) {
            curX += direction.x;
            curY += direction.y;
            if ((targetNode = levelGenerator.getLevelData().getNodeAt(curX, curY)) != null) {
                desiredLength = i; //Distance to the node
                break;
            }
        }

        nodeBounds.setBoarder(GenerationParameters.CORRIDOR_BOARDER);

        while (length < desiredLength) {
            nodeBounds.grow(direction, 1);

            //Ignore parent node, and node we are going to connect to
            if (!levelGenerator.getLevelData().canFit(nodeBounds, parentConnector.owner, targetNode)) {
                break; //Stop, if we can't grow anymore
            }

            length += 1;

            if (length == desiredLength && targetNode != null) {
                this.connect(targetNode);
                this.extended = true;
            }
        }
    }

    @Override
    public boolean validate() {
        return length >= GenerationParameters.CORRIDOR_MIN_LENGTH;
    }

    @Override
    public Connector[] getExtensionConnectors() {
        return new Connector[]{createEndConnector()};
    }

    private void determineDirection(Connector connector) {
        if (connector.fixedDirection) {
            //If corridor is coming out of room, it should follow connectors direction
            direction = connector.direction;
        } else {
            //If corridor is connected to another corridor, it should turn
            if (levelGenerator.random.nextBoolean()) {
                direction = connector.direction.turnClockwise();
            } else {
                direction = connector.direction.turnCounterClockwise();
            }
        }
    }

    private Connector createEndConnector() {
        return new Connector(startX + direction.x * length, startY + direction.y * length, this, false, direction);
    }

    @Override
    public void scale(int scale) {
        super.scale(scale);
        startX = startX * scale;
        startY = startY * scale;
        length = length * scale;
    }

    public int getStartX() {
        return startX;
    }

    public int getStartY() {
        return startY;
    }

    public int getEndX() {
        return startX + direction.x * (length - 1);
    }

    public int getEndY() {
        return startY + direction.y * (length - 1);
    }

    public Direction getDirection() {
        return direction;
    }

    public int getLength() {
        return length;
    }
}
