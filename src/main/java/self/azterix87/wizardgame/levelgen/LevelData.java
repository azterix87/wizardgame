package self.azterix87.wizardgame.levelgen;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by azterix87 on 6/15/2017.
 * Project WizardGame
 */
public class LevelData {
    private List<LevelNode> nodes = new ArrayList<>();
    private List<Artifact> artifacts = new ArrayList<>();

    public void addNode(LevelNode node) {
        nodes.add(node);
    }

    public void removeNode(LevelNode node) {
        node.disconnectAll();
        nodes.remove(node);
    }

    public void addArtifact(Artifact artifact) {
        artifacts.add(artifact);
    }

    public void removeArtifact(Artifact artifact) {
        artifacts.remove(artifact);
    }

    public List<Artifact> findArtifactsByType(String type) {
        List<Artifact> result = new ArrayList<>();
        for (Artifact artifact : artifacts) {
            if (artifact.type.equals(type)) {
                result.add(artifact);
            }
        }
        return artifacts;
    }

    public Artifact findArtifactByType(String type) {
        for (Artifact artifact : artifacts) {
            if (artifact.type.equals(type)) {
                return artifact;
            }
        }
        return null;
    }

    public BaseMap getBasemap(int x, int y, int width, int height) {
        BaseMap baseMap = new BaseMap(width, height);
        NodeBounds baseMapBounds = new NodeBounds(x, y, width, height);
        for (LevelNode node : getNodesWithin(baseMapBounds)) {
            NodeBounds bounds = node.getNodeBounds();
            baseMap.fill(bounds.getX() - x, bounds.getY() - y, bounds.getWidth(), bounds.getHeight(), true);
        }
        return baseMap;
    }

    public LevelNode getNodeAt(int x, int y) {
        for (LevelNode node : nodes) {
            if (node.getNodeBounds().contains(x, y)) {
                return node;
            }
        }
        return null;
    }

    public List<LevelNode> getNodesWithin(NodeBounds bounds) {
        List<LevelNode> result = new ArrayList<>();

        for (LevelNode node : nodes) {
            if (node.getNodeBounds().intersectsIgnoreBoarder(bounds)) {
                result.add(node);
            }
        }

        return result;
    }

    /**
     * returns true if it's possible to place this nodeBounds on the map without collisions with others
     */
    public boolean canFit(NodeBounds bounds, LevelNode... ignoredNodes) {
        List<LevelNode> ignoreList = Arrays.asList(ignoredNodes);
        for (LevelNode node : nodes) {
            if (!ignoreList.contains(node) && node.getNodeBounds().intersects(bounds)) {
                return false;
            }
        }
        return true;
    }

    public int nodeCount() {
        return nodes.size();
    }

    public int roomCount() {
        return getRooms().size();
    }

    public int corridorCount() {
        return getCorridors().size();
    }

    public void scale(int scale) {
        for (LevelNode node : nodes) {
            node.scale(scale);
        }
    }

    public List<LevelNode> getLevelNodes() {
        return nodes;
    }

    public List<Room> getRooms() {
        List<Room> result = new ArrayList<>();
        for (LevelNode node : nodes) {
            if (node instanceof Room) {
                result.add((Room) node);
            }
        }
        return result;
    }

    public List<Corridor> getCorridors() {
        List<Corridor> result = new ArrayList<>();
        for (LevelNode node : nodes) {
            if (node instanceof Corridor) {
                result.add((Corridor) node);
            }
        }
        return result;
    }

}
