package self.azterix87.wizardgame.levelgen;

/**
 * Created by azterix87 on 6/16/2017.
 * Project WizardGame
 */
public class BaseMap {
    private int width, height;
    private int offsetX, offsetY;
    private boolean[][] data;

    public BaseMap(int width, int height) {
        this.width = width;
        this.height = height;
        data = new boolean[width][height];
    }

    public boolean get(int x, int y) {
        if (!checkRange(x - offsetX, y - offsetY)) {
            return false;
        } else {
            return data[x - offsetX][y - offsetY];
        }
    }

    public void set(int x, int y, boolean value) {
        if (checkRange(x - offsetX, y - offsetY)) {
            data[x - offsetX][y - offsetY] = value;
        }
    }

    public void fill(int startX, int startY, int width, int height, boolean value) {
        for (int i = startX; i < startX + width; i++) {
            for (int j = startY; j < startY + height; j++) {
                set(i, j, value);
            }
        }
    }

    private boolean checkRange(int x, int y) {
        return x >= 0 && y >= 0 && x < width && y < height;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getOffsetX() {
        return offsetX;
    }

    public void setOffsetX(int offsetX) {
        this.offsetX = offsetX;
    }

    public int getOffsetY() {
        return offsetY;
    }

    public void setOffsetY(int offsetY) {
        this.offsetY = offsetY;
    }

    /*public BaseMap scale(int scale) {
        BaseMap scaled = new BaseMap(width * scale, height * scale);

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                scaled.fill(i * scale, j * scale, scale, scale, data[i][j]);
            }
        }

        scaled.setOffsetX(offsetX * scale);
        scaled.setOffsetY(offsetY * scale);

        return scaled;
    }*/


}
