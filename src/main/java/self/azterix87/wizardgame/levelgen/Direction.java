package self.azterix87.wizardgame.levelgen;

/**
 * Created by azterix87 on 6/5/2017.
 * Project WizardGame
 */
public enum Direction {
    NORTH(0, 1), EAST(1, 0), SOUTH(0, -1), WEST(-1, 0);

    public int x, y;

    Direction(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public static Direction fromNumber(int dir) {
        dir = dir % 4; //Limit number to range 0, 1, 2, 3
        switch (dir) {
            case 0:
                return NORTH;
            case 1:
                return EAST;
            case 2:
                return SOUTH;
            case 3:
                return WEST;
            default:
                return NORTH;
        }
    }


    public Direction opposite() {
        switch (this) {
            case NORTH:
                return SOUTH;
            case EAST:
                return WEST;
            case SOUTH:
                return NORTH;
            case WEST:
                return EAST;
            default:
                return NORTH;
        }
    }

    public Direction turnClockwise() {
        switch (this) {
            case NORTH:
                return EAST;
            case EAST:
                return SOUTH;
            case SOUTH:
                return WEST;
            case WEST:
                return NORTH;
            default:
                return NORTH;
        }
    }

    public Direction turnCounterClockwise() {
        switch (this) {
            case NORTH:
                return WEST;
            case EAST:
                return NORTH;
            case SOUTH:
                return EAST;
            case WEST:
                return SOUTH;
            default:
                return NORTH;
        }
    }
}
