package self.azterix87.wizardgame.levelgen;


/**
 * Created by azterix87 on 6/3/2017.
 * Project WizardGame
 */
public class Room extends LevelNode {
    private Direction backDirection;

    public Room(LevelGenerator levelGenerator, Connector connector) {
        super(levelGenerator, connector);
        backDirection = connector.direction.opposite();
    }

    @Override
    public void generate() {
        nodeBounds.setBoarder(GenerationParameters.ROOM_BOARDER);

        int desiredGrowNorth = levelGenerator.roomSizeDice.next();
        int desiredGrowSouth = levelGenerator.roomSizeDice.next();
        int desiredGrowEast = levelGenerator.roomSizeDice.next();
        int desiredGrowWest = levelGenerator.roomSizeDice.next();

        int currentGrowNorth = 0;
        int currentGrowSouth = 0;
        int currentGrowEast = 0;
        int currentGrowWest = 0;

        boolean shouldGrow = true;

        while (shouldGrow) {
            shouldGrow = false;
            if (backDirection != Direction.NORTH && currentGrowNorth < desiredGrowNorth) {
                //Try to grow in this direction
                nodeBounds.growNorth(1);

                if (levelGenerator.getLevelData().canFit(nodeBounds, parentConnector.owner)) {
                    currentGrowNorth++;
                    shouldGrow = true;
                } else {
                    nodeBounds.growNorth(-1);
                    desiredGrowNorth = currentGrowNorth;
                }
            }
            if (backDirection != Direction.SOUTH && currentGrowSouth < desiredGrowSouth) {
                //Try to grow in this direction
                nodeBounds.growSouth(1);

                if (levelGenerator.getLevelData().canFit(nodeBounds, parentConnector.owner)) {
                    currentGrowSouth++;
                    shouldGrow = true;
                } else {
                    nodeBounds.growSouth(-1);
                    desiredGrowSouth = currentGrowSouth;
                }
            }
            if (backDirection != Direction.EAST && currentGrowEast < desiredGrowEast) {
                //Try to grow in this direction
                nodeBounds.growEast(1);

                if (levelGenerator.getLevelData().canFit(nodeBounds, parentConnector.owner)) {
                    currentGrowEast++;
                    shouldGrow = true;
                } else {
                    nodeBounds.growEast(-1);
                    desiredGrowEast = currentGrowEast;
                }
            }
            if (backDirection != Direction.WEST && currentGrowWest < desiredGrowWest) {
                //Try to grow in this direction
                nodeBounds.growWest(1);

                if (levelGenerator.getLevelData().canFit(nodeBounds, parentConnector.owner)) {
                    currentGrowWest++;
                    shouldGrow = true;
                } else {
                    nodeBounds.growWest(-1);
                    desiredGrowWest = currentGrowWest;
                }
            }
        }

    }

    @Override
    public boolean validate() {
        return nodeBounds.getWidth() >= GenerationParameters.ROOM_MIN_SIZE && nodeBounds.getHeight() >= GenerationParameters.ROOM_MIN_SIZE;
    }

    @Override
    public Connector[] getExtensionConnectors() {
        int amount = levelGenerator.roomExitingCorridorsDice.next();
        Connector[] connectors = new Connector[amount];

        for (int i = 0; i < connectors.length; i++) {
            connectors[i] = createConnector();
        }

        return connectors;
    }


    private Connector createConnector() {
        Direction dir = Direction.fromNumber(levelGenerator.random.nextInt(4));

        int connectorX = 0, connectorY = 0;

        switch (dir) {
            case NORTH:
                connectorX = nodeBounds.getX() + levelGenerator.random.nextInt(nodeBounds.getWidth() - 2) + 1;
                connectorY = nodeBounds.getEndY() + 1;
                break;
            case SOUTH:
                connectorX = nodeBounds.getX() + levelGenerator.random.nextInt(nodeBounds.getWidth() - 2) + 1;
                connectorY = nodeBounds.getY() - 1;
                break;
            case EAST:
                connectorX = nodeBounds.getEndX() + 1;
                connectorY = nodeBounds.getY() + levelGenerator.random.nextInt(nodeBounds.getHeight() - 2) + 1;
                break;
            case WEST:
                connectorX = nodeBounds.getX() - 1;
                connectorY = nodeBounds.getY() + levelGenerator.random.nextInt(nodeBounds.getHeight() - 2) + 1;
                break;
        }

        return new Connector(connectorX, connectorY, this, true, dir);
    }
}
