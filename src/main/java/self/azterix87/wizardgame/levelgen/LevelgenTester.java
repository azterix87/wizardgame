package self.azterix87.wizardgame.levelgen;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

import java.util.Random;

/**
 * Created by azterix87 on 6/5/2017.
 * Project WizardGame
 */
public class LevelgenTester implements ApplicationListener {
    private LevelGenerator levelGenerator;
    private ShapeRenderer renderer;

    public static void main(String args[]) {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        new LwjglApplication(new LevelgenTester(), config);
    }

    @Override
    public void create() {
        levelGenerator = new LevelGenerator(System.nanoTime());
        levelGenerator.generate(10);
        System.out.println("nodes generated: " + levelGenerator.getLevelData().getLevelNodes().size());

        //levelGenerator.generate(200);
        renderer = new ShapeRenderer();
        OrthographicCamera camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.zoom = 1F;
        camera.update();
        renderer.setProjectionMatrix(camera.combined);
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void render() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        renderer.begin(ShapeRenderer.ShapeType.Point);

        for (LevelNode node : levelGenerator.getLevelData().getLevelNodes()) {
            renderer.setColor(Color.valueOf(Integer.toHexString(1000000 + new Random(node.nodeBounds.getX() * node.nodeBounds.getY()).nextInt())));
            for (int x = node.nodeBounds.getX(); x <= node.nodeBounds.getEndX(); x++) {
                for (int y = node.nodeBounds.getY(); y <= node.nodeBounds.getEndY(); y++) {
                    renderer.point(x, y, 0);
                }
            }
        }
        renderer.end();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
