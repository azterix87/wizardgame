package self.azterix87.wizardgame.levelgen;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by azterix87 on 6/29/2017.
 * Project WizardGame
 */
public class Artifact {
    public final Vector2 position;
    public final String type;
    public final String parameters;

    public Artifact(Vector2 position, String type, String parameters) {
        this.position = position;
        this.type = type;
        this.parameters = parameters;
    }
}
