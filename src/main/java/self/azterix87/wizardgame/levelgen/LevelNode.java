package self.azterix87.wizardgame.levelgen;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by azterix87 on 6/7/2017.
 * Project WizardGame
 */
public abstract class LevelNode {
    protected final NodeBounds nodeBounds;
    protected final LevelGenerator levelGenerator;
    protected final List<LevelNode> connections = new ArrayList<>();
    protected final Connector parentConnector;
    protected boolean extended;

    public LevelNode(LevelGenerator levelGenerator, Connector connector) {
        this.levelGenerator = levelGenerator;
        this.parentConnector = connector;
        this.nodeBounds = new NodeBounds(connector.x, connector.y, 1, 1);
    }

    public abstract void generate();

    public abstract boolean validate();

    public abstract Connector[] getExtensionConnectors();

    public boolean isExtended() {
        return extended;
    }

    public void setExtended(boolean extended) {
        this.extended = extended;
    }

    public NodeBounds getNodeBounds() {
        return nodeBounds;
    }

    public boolean isConnected(LevelNode node) {
        return connections.contains(node);
    }

    public List<LevelNode> getConnections() {
        return connections;
    }

    public void connect(LevelNode another) {
        connections.add(another);
        another.connections.add(this);
    }

    public void disconnect(LevelNode another) {
        connections.remove(another);
        another.connections.remove(this);
    }

    public void disconnectAll() {
        for (LevelNode node : new ArrayList<>(connections)) {
            disconnect(node);
        }
    }

    public void scale(int scale) {
        nodeBounds.scaleAndMove(scale);
    }
}
