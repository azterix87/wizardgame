package self.azterix87.wizardgame.levelgen;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by azterix87 on 6/3/2017.
 * Project WizardGame
 */
public class LevelGenerator {
    protected Random random;
    protected Dice corridorLengthDice;
    protected Dice roomSizeDice;
    protected Dice roomExitingCorridorsDice;
    protected Chance corridorTurnChance;
    private long seed;
    private LevelData levelData;
    //private List<LevelNode> levelNodes = new ArrayList<>();

    public LevelGenerator(long seed) {
        this.seed = seed;
        this.random = new Random(seed);

        corridorLengthDice = new Dice(GenerationParameters.CORRIDOR_MIN_LENGTH, GenerationParameters.CORRIDOR_MAX_LENGTH, random);
        roomSizeDice = new Dice(GenerationParameters.ROOM_MIN_SIZE, GenerationParameters.ROOM_MAX_SIZE, random);
        roomExitingCorridorsDice = new Dice(1, 5, random);
        corridorTurnChance = new Chance(GenerationParameters.CORRIDOR_TURN_CHANCE, random);
    }

    public void generate(int desiredRooms) {
        levelData = new LevelData();

        Connector entryConnector = new Connector(0, 0, null, true, Direction.NORTH);
        Room room1 = new Room(this, entryConnector);
        room1.generate();
        levelData.addNode(room1);

        int previousNodeAmount = levelData.nodeCount();
        boolean stuck = false;

        while (levelData.roomCount() < desiredRooms && !stuck) {

            for (LevelNode node : new ArrayList<>(levelData.getLevelNodes())) {
                if (!node.isExtended()) {
                    extendNode(node);
                }
            }

            if (previousNodeAmount < levelData.nodeCount()) {
                previousNodeAmount = levelData.nodeCount();
            } else {
                stuck = true;
            }
        }

        //If generation is stuck, but we didn't get required amount of nodes, restart generation
        if (stuck) {
            generate(desiredRooms);
        }
    }

    private void extendNode(LevelNode node) {
        for (Connector connector : node.getExtensionConnectors()) {
            LevelNode newNode;

            if (node instanceof Corridor && !corridorTurnChance.next()) {
                newNode = new Room(this, connector);
            } else {
                newNode = new Corridor(this, connector);
            }

            newNode.generate();
            if (validateNode(newNode)) {
                levelData.addNode(newNode);
                node.connect(newNode);
            } else {
                newNode.disconnectAll();
            }
        }
        node.setExtended(true);
    }

    private boolean validateNode(LevelNode levelNode) {
        return levelNode.validate();
    }

    /*private void cleanUp() {
        boolean hasChanged = true;

        while (hasChanged) {
            hasChanged = false;
            Iterator<LevelNode> i = levelNodes.listIterator();

            while (i.hasNext()) {
                LevelNode node = i.next();
                if (node instanceof Corridor && node.connections.size() < 2) {
                    node.disconnectAll();
                    i.remove();
                    hasChanged = true;
                }
            }
        }
    }*/

    public LevelData getLevelData() {
        return levelData;
    }

    public long getSeed() {
        return seed;
    }
}
