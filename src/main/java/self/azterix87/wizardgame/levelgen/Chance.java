package self.azterix87.wizardgame.levelgen;

import java.util.Random;

/**
 * Created by azterix87 on 6/3/2017.
 * Project WizardGame
 */
public class Chance {
    private float probability;
    private Random random;

    public Chance(float probability, Random random) {
        this.probability = probability;
        this.random = random;
    }

    public boolean next() {
        return random.nextFloat() < probability;
    }
}
