package self.azterix87.wizardgame.levelgen;

import self.azterix87.seraphim.tiledmap.Chunk;
import self.azterix87.seraphim.tiledmap.Layer;
import self.azterix87.seraphim.tiledmap.TileRegistry;

/**
 * Created by azterix87 on 6/26/2017.
 * Project WizardGame
 */
public class ChunkGenerator {
    private Chunk chunk;
    private BaseMap basemap;
    private TileRegistry tileRegistry;

    public ChunkGenerator(int x, int y, int width, int height, BaseMap baseMap, TileRegistry tileRegistry) {
        this.chunk = new Chunk(x, y, width, height);
        this.tileRegistry = tileRegistry;
        this.basemap = baseMap;
    }

    public Chunk generate() {
        generateFloor();
        generateWalls();
        generateRoof();
        generateCollisionMap();


        return chunk;
    }

    private void generateFloor() {
        //Create floor layer with sorting index -16
        Layer floorLayer = new Layer(chunk.getWidth(), chunk.getHeight(), -16);

        //Find required tiles ids
        int tileFloor = tileRegistry.getTileId("tile.floor");
        int tileFloorTop = tileRegistry.getTileId("tile.floor_top");

        //Generate floor
        for (int i = 0; i < chunk.getWidth(); i++) {
            for (int j = 0; j < chunk.getHeight(); j++) {
                //If baseMap is true at this point, we need a floor tile
                if (basemap.get(i, j)) {
                    //If there is a tile on top of this tile, it has to be regular "floor" otherwise a "floor_top"
                    if (basemap.get(i, j + 1)) {
                        floorLayer.setTileAt(i, j, tileFloor);
                    } else {
                        floorLayer.setTileAt(i, j, tileFloorTop);
                    }
                }
            }
        }

        chunk.addLayer(floorLayer);
    }

    private void generateWalls() {
        //Create two layers for walls
        Layer walls1 = new Layer(chunk.getWidth(), chunk.getHeight(), -15);
        Layer walls2 = new Layer(chunk.getWidth(), chunk.getHeight(), -14);
        //The second layer of walls has to be offset by one tile upwards (well, not to cover wall underneath :D)
        walls2.setYOffset(1);

        //Find required tiles ids
        int tileWallBottomLeft = tileRegistry.getTileId("tile.wall_bottom_left");
        int tileWallBottomMiddle = tileRegistry.getTileId("tile.wall_bottom_middle");
        int tileWallBottomRight = tileRegistry.getTileId("tile.wall_bottom_right");
        int tileWallLeft = tileRegistry.getTileId("tile.wall_left");
        int tileWallMiddle = tileRegistry.getTileId("tile.wall_middle");
        int tileWallRight = tileRegistry.getTileId("tile.wall_right");
        int tileWallBack = tileRegistry.getTileId("tile.wall_back");

        for (int i = 0; i < chunk.getWidth(); i++) {
            for (int j = 0; j < chunk.getHeight(); j++) {
                if (!basemap.get(i, j)) {
                    if (basemap.get(i, j - 1)) {
                        walls1.setTileAt(i, j, tileWallBottomMiddle);
                        walls2.setTileAt(i, j, tileWallMiddle);
                    }
                    if (basemap.get(i, j - 1) && !basemap.get(i - 1, j - 1)) {
                        walls1.setTileAt(i, j, tileWallBottomLeft);
                        walls2.setTileAt(i, j, tileWallLeft);
                    }
                    if (basemap.get(i, j - 1) && !basemap.get(i + 1, j - 1)) {
                        walls1.setTileAt(i, j, tileWallBottomRight);
                        walls2.setTileAt(i, j, tileWallRight);
                    }
                }
            }
        }

        chunk.addLayer(walls1);
        chunk.addLayer(walls2);
    }

    private void generateRoof() {
        //Create roof layer with sorting index 16 (Above entities)
        Layer roofLayer = new Layer(chunk.getWidth(), chunk.getHeight(), 16);
        roofLayer.setYOffset(2);

        //Find out required tile ids
        int roofBottom = tileRegistry.getTileId("tile.roof_bottom");
        int roofBottomLeft = tileRegistry.getTileId("tile.roof_bottom_left");
        int roofBottomRight = tileRegistry.getTileId("tile.roof_bottom_right");
        int roofTop = tileRegistry.getTileId("tile.roof_top");
        int roofTopLeft = tileRegistry.getTileId("tile.roof_top_left");
        int roofTopRight = tileRegistry.getTileId("tile.roof_top_right");
        int roofLeft = tileRegistry.getTileId("tile.roof_left");
        int roofRight = tileRegistry.getTileId("tile.roof_right");
        int roofInnerTL = tileRegistry.getTileId("tile.roof_inner_top_left");
        int roofInnerTR = tileRegistry.getTileId("tile.roof_inner_top_right");
        int roofInnerBL = tileRegistry.getTileId("tile.roof_inner_bottom_left");
        int roofInnerBR = tileRegistry.getTileId("tile.roof_inner_bottom_right");
        int roofEmpty = tileRegistry.getTileId("tile.roof_empty");


        //Fill roof layer with empty tiles
        for (int i = 0; i < chunk.getWidth(); i++) {
            for (int j = 0; j < chunk.getHeight(); j++) {
                if (!basemap.get(i, j)) {
                    roofLayer.setTileAt(i, j, roofEmpty);
                }
            }
        }

        //Generate roof
        for (int i = 0; i < chunk.getWidth(); i++) {
            for (int j = 0; j < chunk.getHeight(); j++) {

                if (!basemap.get(i, j)) {
                    if (basemap.get(i, j - 1)) {
                        roofLayer.setTileAt(i, j, roofTop);
                    }
                    if (basemap.get(i, j + 1)) {
                        roofLayer.setTileAt(i, j, roofBottom);
                    }
                    if (basemap.get(i + 1, j)) {
                        roofLayer.setTileAt(i, j, roofLeft);
                    }
                    if (basemap.get(i - 1, j)) {
                        roofLayer.setTileAt(i, j, roofRight);
                    }

                    if (basemap.get(i + 1, j + 1) && !basemap.get(i + 1, j) && !basemap.get(i, j + 1)) {
                        roofLayer.setTileAt(i, j, roofBottomLeft);
                    }
                    if (basemap.get(i - 1, j + 1) && !basemap.get(i - 1, j) && !basemap.get(i, j + 1)) {
                        roofLayer.setTileAt(i, j, roofBottomRight);
                    }
                    if (basemap.get(i - 1, j - 1) && !basemap.get(i - 1, j) && !basemap.get(i, j - 1)) {
                        roofLayer.setTileAt(i, j, roofTopRight);
                    }
                    if (basemap.get(i + 1, j - 1) && !basemap.get(i + 1, j) && !basemap.get(i, j - 1)) {
                        roofLayer.setTileAt(i, j, roofTopLeft);
                    }


                    if (basemap.get(i, j + 1) && basemap.get(i + 1, j) && basemap.get(i + 1, j + 1)) {
                        roofLayer.setTileAt(i, j, roofInnerTR);
                    }
                    if (basemap.get(i + 1, j) && basemap.get(i + 1, j - 1) && basemap.get(i, j - 1)) {
                        roofLayer.setTileAt(i, j, roofInnerBR);
                    }
                    if (basemap.get(i - 1, j) && basemap.get(i - 1, j - 1) && basemap.get(i, j - 1)) {
                        roofLayer.setTileAt(i, j, roofInnerBL);
                    }
                    if (basemap.get(i - 1, j) && basemap.get(i - 1, j + 1) && basemap.get(i, j + 1)) {
                        roofLayer.setTileAt(i, j, roofInnerTL);
                    }
                }
            }
        }

        chunk.addLayer(roofLayer);
    }

    private void generateCollisionMap() {
        boolean[][] collisionMap = new boolean[chunk.getWidth()][chunk.getHeight()];

        for (int i = 0; i < chunk.getWidth(); i++) {
            for (int j = 0; j < chunk.getHeight(); j++) {
                //If tile has no floor, but, it has floor nearby it has to be a collider
                if (!basemap.get(i, j)
                        && (basemap.get(i - 1, j) || basemap.get(i - 1, j + 1)
                        || basemap.get(i, j + 1) || basemap.get(i + 1, j + 1)
                        || basemap.get(i + 1, j) || basemap.get(i + 1, j - 1)
                        || basemap.get(i, j - 1) || basemap.get(i - 1, j - 1))) {
                    collisionMap[i][j] = true;
                }
            }
        }

        chunk.setCollisionMap(collisionMap);
    }


}
