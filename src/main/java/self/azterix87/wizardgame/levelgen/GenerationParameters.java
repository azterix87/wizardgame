package self.azterix87.wizardgame.levelgen;

/**
 * Created by azterix87 on 6/3/2017.
 * Project WizardGame
 */
public class GenerationParameters {
    public static final int CORRIDOR_MIN_LENGTH = 3;
    public static final int CORRIDOR_MAX_LENGTH = 7;
    public static final int CORRIDOR_BOARDER = 2;
    public static final int ROOM_BOARDER = 2;
    public static final int ROOM_MIN_SIZE = 3;
    public static final int ROOM_MAX_SIZE = 5;

    public static final float CORRIDOR_TURN_CHANCE = 0.5F;


}
