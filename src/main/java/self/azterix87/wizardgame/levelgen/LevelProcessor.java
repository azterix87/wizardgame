package self.azterix87.wizardgame.levelgen;

import com.badlogic.gdx.math.Vector2;
import self.azterix87.seraphim.tiledmap.Tile;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by azterix87 on 6/28/2017.
 * Project WizardGame
 */
public class LevelProcessor {
    private LevelData levelData;


    public LevelProcessor(LevelData levelData) {
        this.levelData = levelData;
    }

    public void removeDeadEnds() {
        List<Corridor> corridorsToRemove = new ArrayList<>();
        boolean moreDeadEnds = true;
        while (moreDeadEnds) {
            moreDeadEnds = false;

            for (Corridor corridor : levelData.getCorridors()) {
                if (corridor.getConnections().size() < 2) {
                    corridorsToRemove.add(corridor);
                    moreDeadEnds = true;
                }
            }

            for (Corridor corridor : corridorsToRemove) {
                levelData.removeNode(corridor);
            }
            corridorsToRemove.clear();
        }
    }

    public void setSpawnPoint() {
        Room room = levelData.getRooms().get(0);
        System.out.println(room.getNodeBounds());
        float spawnX = (room.getNodeBounds().getX() + room.getNodeBounds().getWidth() / 2F) * Tile.SIZE;
        float spawnY = (room.getNodeBounds().getY() + room.getNodeBounds().getHeight() / 2F) * Tile.SIZE;
        levelData.addArtifact(new Artifact(new Vector2(spawnX, spawnY), "player_spawn", ""));
    }

}
