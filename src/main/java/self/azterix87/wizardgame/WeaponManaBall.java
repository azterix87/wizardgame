package self.azterix87.wizardgame;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import self.azterix87.seraphim.GameContext;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by azterix87 on 2/1/17.
 * Project WizardGame
 */
public class WeaponManaBall extends Weapon {
    private static final float FLY_SPEED = 500;
    private static final float KNOCKBACK = 5;

    public WeaponManaBall(float coolDown, int minDamage, int maxDamage) {
        super("ManaBall", coolDown, minDamage, maxDamage);
    }

    @Override
    public void fire(GameContext gameContext, Entity owner, Vector2 position, Vector2 attackDirection) {
        super.fire(gameContext, owner, position, attackDirection);

        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("velX", Float.toString(attackDirection.x * FLY_SPEED));
        parameters.put("velY", Float.toString(attackDirection.y * FLY_SPEED));
        parameters.put("damage", Integer.toString(getRandomDamage()));
        parameters.put("knockBack", Float.toString(KNOCKBACK));
        gameContext.spawnEntity("mana_ball", new Vector3(position, 0), parameters);
    }
}
