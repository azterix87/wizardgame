package self.azterix87.wizardgame;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import self.azterix87.seraphim.GameContext;
import self.azterix87.seraphim.InputAdapterController;
import self.azterix87.seraphim.components.LivingStats;
import self.azterix87.seraphim.components.LivingStatsListener;
import self.azterix87.seraphim.components.Transform;
import self.azterix87.seraphim.systems.AnimationPlayer;
import self.azterix87.seraphim.systems.motionanimation.MotionAnimator;
import self.azterix87.seraphim.systems.physics.PhysicalBody;
import self.azterix87.seraphim.systems.spriterendering.SpriteRenderable;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by azterix87 on 6/1/2017.
 * Project WizardGame
 */

//Todo rewrite this class
public class PlayerController extends InputAdapterController implements LivingStatsListener {
    private static final float walkForceMultiplier = 100;
    private Entity player;
    private float speed;
    private float minWalkingSpeed = 20F;
    private Weapon primary;
    private Weapon secondary;
    private GameContext gameContext;
    private Vector2 moveDir;

    public PlayerController(GameContext gameContext, Entity player) {
        this.player = player;
        this.gameContext = gameContext;
        LivingStats.mapper.get(player).addListener(this);
        moveDir = new Vector2();
        speed = 8;
        primary = new WeaponMelee(0.3F, 5, 15, 20, 5);
        secondary = new WeaponManaBall(1.5F, 13, 19);
        updateAnimation();
    }

    public void update(float delta) {
        PhysicalBody.mapper.get(player).applyForce(moveDir.cpy().nor().scl(speed * walkForceMultiplier));
    }

    private void updateAnimation() {
        SpriteRenderable spriteRenderable = SpriteRenderable.mapper.get(player);
        AnimationPlayer animationPlayer = spriteRenderable.getAnimationPlayer();

        if (moveDir.x < 0) {
            animationPlayer.setAnimationAndPlay("assets/animations/temp_player_walk_left.animation");
        } else if (moveDir.x > 0) {
            animationPlayer.setAnimationAndPlay("assets/animations/temp_player_walk_right.animation");
        } else if (moveDir.y < 0) {
            animationPlayer.setAnimationAndPlay("assets/animations/temp_player_walk_down.animation");
        } else if (moveDir.y > 0) {
            animationPlayer.setAnimationAndPlay("assets/animations/temp_player_walk_up.animation");
        } else {
            animationPlayer.setAnimationAndPlay("assets/animations/temp_player_idle.animation");
        }
    }

    public Weapon getPrimaryWeapon() {
        return primary;
    }

    public void setPrimaryWeapon(Weapon primary) {
        this.primary = primary;
    }

    public Weapon getSecondaryWeapon() {
        return secondary;
    }

    public void setSecondaryWeapon(Weapon secondary) {
        this.secondary = secondary;
    }

    @Override
    public void onEntityDamaged(float amount, LivingStats livingStats) {
        Gdx.app.debug("@Player", "player was damaged by " + amount);
        MotionAnimator.mapper.get(player).setAnimationAndPlay("assets/motionAnimations/damaged.manim");

        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("damage", Integer.toString(Math.round(amount)));
        gameContext.spawnEntity("damage_particle", Transform.mapper.get(player).localPosition.cpy().add(0, 40, 0), parameters);
    }

    @Override
    public void onEntityHealed(float amount, LivingStats livingStats) {

    }

    @Override
    public void onEntityDied(LivingStats livingStats) {

    }

    @Override
    public boolean keyDown(int keycode) {
        if (keycode == Input.Keys.W) {
            moveDir.y += 1;
        } else if (keycode == Input.Keys.S) {
            moveDir.y -= 1;
        } else if (keycode == Input.Keys.A) {
            moveDir.x -= 1;
        } else if (keycode == Input.Keys.D) {
            moveDir.x += 1;
        } else {
            return false;
        }

        updateAnimation();
        return true;
    }

    @Override
    public boolean keyUp(int keycode) {
        if (keycode == Input.Keys.W) {
            moveDir.y -= 1;
        } else if (keycode == Input.Keys.S) {
            moveDir.y += 1;
        } else if (keycode == Input.Keys.A) {
            moveDir.x += 1;
        } else if (keycode == Input.Keys.D) {
            moveDir.x -= 1;
        } else {
            return false;
        }
        updateAnimation();
        return true;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        Vector3 clickPoint = gameContext.getMainCamera().unproject(new Vector3(screenX, screenY, 0));
        Vector3 playerPos = Transform.mapper.get(player).localPosition;
        Vector2 attackDir = new Vector2(clickPoint.x - playerPos.x, clickPoint.y - playerPos.y).nor();

        if (button == Input.Buttons.LEFT) {
            if (primary != null && primary.isReady()) {
                primary.fire(gameContext, player, new Vector2(playerPos.x, playerPos.y), attackDir);
            }
        } else {
            if (secondary != null && secondary.isReady()) {
                secondary.fire(gameContext, player, new Vector2(playerPos.x, playerPos.y), attackDir);
                return true;
            }
        }

        return false;
    }
}
