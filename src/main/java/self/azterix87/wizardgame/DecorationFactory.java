package self.azterix87.wizardgame;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.math.Vector3;
import self.azterix87.seraphim.ParametricEntityFactory;
import self.azterix87.seraphim.components.Transform;
import self.azterix87.seraphim.systems.lighting.Lightmap;
import self.azterix87.seraphim.systems.spriterendering.SpriteRenderable;

import java.util.Map;

/**
 * Created by azterix87 on 6/28/2017.
 * Project WizardGame
 */
public class DecorationFactory implements ParametricEntityFactory {
    @Override
    public Entity createEntity(String type, Vector3 position, Map<String, String> parameters) {
        if (type.equals("magic_lantern")) {
            return createMagicLantern(position);
        } else {
            return null;
        }
    }

    @Override
    public String[] getSupportedEntityTypes() {
        return new String[]{"magic_lantern"};
    }

    private Entity createMagicLantern(Vector3 position) {
        Entity lantern = new Entity();

        Transform transform = new Transform();
        transform.localPosition.set(position);
        lantern.add(transform);

        SpriteRenderable renderable = new SpriteRenderable();
        renderable.getAnimationPlayer().setAnimationAndPlay("assets/animations/magic_lamp.animation");
        renderable.setOriginX(16);
        renderable.setOriginY(16);
        lantern.add(renderable);

        Lightmap lightmap = new Lightmap("assets/textures/torch_light.png");
        lightmap.setScale(3);
        lantern.add(lightmap);

        return lantern;
    }
}
