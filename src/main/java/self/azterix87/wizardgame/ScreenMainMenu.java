package self.azterix87.wizardgame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import self.azterix87.seraphim.ScreenManager;
import self.azterix87.seraphim.assets.Assets;
import self.azterix87.seraphim.gui.BasicLayout;
import self.azterix87.seraphim.gui.GUIScreen;

/**
 * Created by azterix87 on 6/1/2017.
 * Project WizardGame
 */
public class ScreenMainMenu extends GUIScreen {
    public ScreenMainMenu(Assets assets, ScreenManager screenManager) {
        super();
        Skin skin = assets.getAsset("assets/skins/default/uiskin.json", Skin.class);

        Table root = new Table();
        root.setFillParent(true);
        root.defaults().space(5);

        TextButton bPlay = new TextButton("Play", skin);
        TextButton bSettings = new TextButton("Settings", skin);
        TextButton bHelp = new TextButton("Help", skin);
        TextButton bSaveLoad = new TextButton("Save/Load", skin);
        TextButton bExit = new TextButton("Exit", skin);

        root.add(bPlay);
        root.row();
        root.add(bSettings);
        root.row();
        root.add(bHelp);
        root.row();
        root.add(bSaveLoad);
        root.row();
        root.add(bExit);

        gui.setLayout(new BasicLayout(root));

        bPlay.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                screenManager.setCurrentScreen("game");
            }
        });

        bExit.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                Gdx.app.exit();
            }
        });

        bSettings.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                screenManager.setCurrentScreen("settings");
            }
        });
    }
}
