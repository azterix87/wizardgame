package self.azterix87.wizardgame;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import self.azterix87.seraphim.ParametricEntityFactory;
import self.azterix87.seraphim.components.LivingStats;
import self.azterix87.seraphim.components.Tags;
import self.azterix87.seraphim.components.Transform;
import self.azterix87.seraphim.systems.collision.Collider;
import self.azterix87.seraphim.systems.collision.ColliderFactory;
import self.azterix87.seraphim.systems.dynamics.Dynamics;
import self.azterix87.seraphim.systems.fonts.Text;
import self.azterix87.seraphim.systems.lighting.Lightmap;
import self.azterix87.seraphim.systems.motionanimation.MotionAnimator;
import self.azterix87.seraphim.systems.physics.PhysicalBody;
import self.azterix87.seraphim.systems.physics.PushOutCollisionHandler;
import self.azterix87.seraphim.systems.spriterendering.SpriteRenderable;

import java.util.Map;

/**
 * Created by azterix87 on 6/1/2017.
 * Project WizardGame
 */
public class PlayerFactory implements ParametricEntityFactory {
    @Override
    public Entity createEntity(String type, Vector3 position, Map<String, String> parameters) {
        if (type.equals("player")) {
            Entity player = new Entity();

            Transform transform = new Transform();
            transform.localPosition.set(position.x, position.y, 0);
            player.add(transform);

            SpriteRenderable renderer = new SpriteRenderable();
            renderer.setSprite("assets/textures/player2.png");
            renderer.setOriginX(24);
            renderer.setOriginY(4);
            player.add(renderer);

            player.add(new MotionAnimator());

            PhysicalBody physicalBody = new PhysicalBody();
            physicalBody.setDrag(7);
            player.add(physicalBody);

            Collider collider = ColliderFactory.getCircleCollider(16, 0, 0);
            collider.addCollisionListener(new PushOutCollisionHandler());
            player.add(collider);
            LivingStats livingStats = new LivingStats(100);
            player.add(livingStats);

            Lightmap lightmap = new Lightmap("assets/textures/player_light.png");
            lightmap.setScale(1F);
            player.add(lightmap);

            Text text = new Text("regular");
            text.setText("Player");
            text.setOffset(new Vector2(-16, 70));
            player.add(text);

            Tags tags = new Tags();
            tags.addTag("player");
            player.add(tags);

            player.add(new Dynamics());

            return player;
        }
        return null;
    }

    @Override
    public String[] getSupportedEntityTypes() {
        return new String[]{"player"};
    }
}
