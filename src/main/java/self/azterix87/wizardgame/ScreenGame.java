package self.azterix87.wizardgame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import self.azterix87.seraphim.ScreenManager;
import self.azterix87.seraphim.assets.Assets;

/**
 * Created by azterix87 on 6/1/2017.
 * Project WizardGame
 */
public class ScreenGame implements Screen {
    private boolean first = true;
    private InputMultiplexer input;
    private Game game;

    public ScreenGame(WizardGame wizardGame, ScreenManager screenManager, Assets assets) {
        this.game = new Game(wizardGame, screenManager, assets);
        input = new InputMultiplexer();
        input.addProcessor(game.getInputProcessor());
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(input);
        if (first) {
            game.init();
            game.startGame();
        } else {
            //game.continue...
        }
    }

    @Override
    public void hide() {
        Gdx.input.setInputProcessor(null);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        game.updateAndRender(delta);
    }

    @Override
    public void resize(int width, int height) {
        game.resize(width, height);
    }

    @Override
    public void pause() {
        game.resume();
    }

    @Override
    public void resume() {
        game.pause();
    }

    @Override
    public void dispose() {

    }
}
