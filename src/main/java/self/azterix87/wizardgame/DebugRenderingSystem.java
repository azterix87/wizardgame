package self.azterix87.wizardgame;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Rectangle;
import self.azterix87.seraphim.RenderContext;
import self.azterix87.seraphim.components.Transform;
import self.azterix87.seraphim.systems.collision.Collider;

/**
 * Created by azterix87 on 1/22/17.
 * Project SeraphimEngine
 */
public class DebugRenderingSystem extends IteratingSystem {
    private ShapeRenderer shapeRenderer;
    private RenderContext renderContext;

    public DebugRenderingSystem(RenderContext renderContext) {
        super(Family.all(Transform.class, Collider.class).get(), Integer.MAX_VALUE);
        this.renderContext = renderContext;
        shapeRenderer = new ShapeRenderer();
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        Transform transform = Transform.mapper.get(entity);
        Collider collider = Collider.mapper.get(entity);

        Gdx.gl.glDisable(GL20.GL_BLEND);

        shapeRenderer.setProjectionMatrix(renderContext.getMainCamera().combined);
        shapeRenderer.setTransformMatrix(transform.getTransformMatrix());

        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);

        shapeRenderer.setColor(Color.OLIVE);

        if (collider.getMask() instanceof Circle) {
            Circle mask = (Circle) collider.getMask();
            shapeRenderer.circle(mask.x, mask.y, ((Circle) collider.getMask()).radius);
        } else if (collider.getMask() instanceof Rectangle) {
            Rectangle rect = (Rectangle) collider.getMask();
            shapeRenderer.rect(rect.x, rect.y, rect.width, rect.height);
        }
        shapeRenderer.end();

    }
}
