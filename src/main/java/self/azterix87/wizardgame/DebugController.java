package self.azterix87.wizardgame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector3;
import self.azterix87.seraphim.GameContext;
import self.azterix87.seraphim.InputAdapterController;

/**
 * Created by azterix87 on 6/3/2017.
 * Project WizardGame
 */
public class DebugController extends InputAdapterController {
    GameContext gameContext;


    public DebugController(int inputPriority, GameContext gameContext) {
        super(inputPriority);
        this.gameContext = gameContext;
    }

    @Override
    public void update(float delta) {

    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if (button == 1 && Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT)) {
            Vector3 clickPoint = gameContext.getMainCamera().unproject(new Vector3(screenX, screenY, 0));
            int clickPointX = (int) Math.floor(clickPoint.x / 32) * 32;
            int clickPointY = (int) Math.floor(clickPoint.y / 32) * 32;
            if (Gdx.input.isKeyPressed(Input.Keys.NUM_1)) {
                gameContext.spawnEntity("melee_mob", clickPoint.x,
                        clickPoint.y, null);
                //Gdx.app.log("@Debug", "Click localPosition " + Math.round(clickPoint.x/32)*32 + ", " + Math.round(clickPoint.y/32)*32);
            }
            if (Gdx.input.isKeyPressed(Input.Keys.NUM_2)) {
                gameContext.spawnEntity("magic_lantern", clickPointX, clickPointY, null);
            }
            return true;
        }

        return false;
    }
}
